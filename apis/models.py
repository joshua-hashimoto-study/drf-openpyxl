from django.db import models


class Invoice(models.Model):
    content = models.TextField()
    price = models.IntegerField()
    people = models.IntegerField()

    def __str__(self):
        return self.content