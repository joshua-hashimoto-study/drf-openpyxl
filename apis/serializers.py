from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Invoice


class InvoiceSerializer(ModelSerializer):

    class Meta:
        model = Invoice
        fields = (
            'id',
            'content',
            'price',
            'people',
        )