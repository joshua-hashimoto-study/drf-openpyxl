import io
import uuid
from pathlib import Path

from django.conf import settings
from django.http import FileResponse
from rest_framework.views import APIView
import openpyxl as xl
from openpyxl.writer.excel import save_virtual_workbook


filepath = settings.BASE_DIR / 'static/files/invoices.xlsx'


class DownloadAPIView(APIView):

    def get(self, request, format=None):
        wb = xl.load_workbook(filepath)
        ws = wb['Sheet1']
        data = str(uuid.uuid4())
        ws['A1'].value = data
        byte_file = io.BytesIO(save_virtual_workbook(wb)) 
        return FileResponse(byte_file, filename=f'{data}.xlsx')
