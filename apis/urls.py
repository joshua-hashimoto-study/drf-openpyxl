from django.urls import path

from .views import DownloadAPIView

app_name = 'apis'

urlpatterns = [
    path('download/', DownloadAPIView.as_view(), name='download'),
]