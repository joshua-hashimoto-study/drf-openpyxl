# jstats-drf
API Backend for J-Stats Application.

## docker-compose.yml

```yml
version: "3"

services:
    api:
        build: .
        command: gunicorn core.wsgi -b 0.0.0.0:8000  # gunicorn対応
        environment: 
            APPLICATION_NAME: jstats-api
            ENVIRONMENT: development
            SECRET_KEY: '=oc8fa0!+(4k#1o(7t7j(p(+#!=0*#3&jcw_j(&n7esqve0t!x'
            DEBUG: 1
        volumes:
            - .:/app
        ports:
            - 8000:8000
        depends_on:
            - api_database
    api_database:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"

volumes:
    postgres_data:
```
